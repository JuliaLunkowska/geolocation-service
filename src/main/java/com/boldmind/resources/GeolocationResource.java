package com.boldmind.resources;

import com.boldmind.dao.GeolocationDao;
import com.boldmind.dto.GeolocationDto;
import com.boldmind.model.DbGeolocation;
// import com.boldmind.services.LocationService;
import com.boldmind.session.SessionKeyProvider;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/data")
@Produces(MediaType.APPLICATION_JSON)
public class GeolocationResource {

    private final GeolocationDao geolocationDao;

    @Inject
    public GeolocationResource(final GeolocationDao geolocationDao) {
        this.geolocationDao = geolocationDao;
    }
    
    @POST
    @Path("/geolocation/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response setLocation(GeolocationDto geoLocDto)
    {
    	DbGeolocation geolocation = new DbGeolocation();
    	
    	geolocation.setName(geoLocDto.getName());
    	geolocation.setNumberOfScreens(geoLocDto.getNumberOfScreens());
    	geolocation.setAddress(geoLocDto.getAddress());
    	geolocation.setLocation(geoLocDto.getLocation());
    	geolocation.setDistrict(geoLocDto.getDistrict());
    	geolocation.setDistrictNo(geoLocDto.getDistrictNo());
    	geolocation.setSiteNo(geoLocDto.getSiteNo());
    	geolocation.setPanelNo(geoLocDto.getPanelNo());
    	geolocation.setPostcode(geoLocDto.getPostcode());
    	geolocation.setSize(geoLocDto.getSize());
    	geolocation.setPanelID(geoLocDto.getPanelID());
    	geolocation.setLatitude(geoLocDto.getLatitude());
    	geolocation.setLongitude(geoLocDto.getLongitude());
    	geolocation.setEasting(geoLocDto.getEasting());
    	geolocation.setNorthing(geoLocDto.getNorthing());
    	geolocation.setContractor(geoLocDto.getContractor());
    	geolocation.setTVAreaName(geoLocDto.getTVAreaName());
    	geolocation.setAreaCode(geoLocDto.getAreaCode());
    	geolocation.setAreaName(geoLocDto.getAreaName());
    	geolocation.setTVArea(geoLocDto.getTVArea());
    	geolocation.setSubAreaCode(geoLocDto.getSubAreaCode());
    	geolocation.setSiteCode(geoLocDto.getSiteCode());
    	geolocation.setPanelCode(geoLocDto.getPanelCode());
    	geolocation.setEnvironment(geoLocDto.getEnvironment());
    	geolocation.setPanelName(geoLocDto.getPanelName());
    	geolocation.setFrameID(geoLocDto.getFrameID());
    	geolocation.setPanelSizeName(geoLocDto.getPanelSizeName());
    	geolocation.setPanelFullDescription(geoLocDto.getPanelFullDescription());
    	geolocation.setPanelSizeCode(geoLocDto.getPanelSizeCode());
    	geolocation.setPanelSiteAddressPostcode(geoLocDto.getPanelSiteAddressPostcode());
    	geolocation.setPanelGridReferenceGridn(geoLocDto.getPanelGridReferenceGridn());
    	geolocation.setPanelGridReferenceGride(geoLocDto.getPanelGridReferenceGride());
    	geolocation.setPanelPostarFrameID(geoLocDto.getPanelPostarFrameID());
    	geolocation.setPanelPostarMapId(geoLocDto.getPanelPostarMapId());
    	geolocation.setPanelPostarFramePositionId(geoLocDto.getPanelPostarFramePositionId());
    	geolocation.setPanelPostarSubEnvironment(geoLocDto.getPanelPostarSubEnvironment());
    	geolocation.setPanelRef(geoLocDto.getPanelRef());
    	geolocation.setProduct(geoLocDto.getProduct());
    	geolocation.setOrientation(geoLocDto.getOrientation());
    	geolocation.setConurbation(geoLocDto.getConurbation());
    	geolocation.setSubAreaName(geoLocDto.getSubAreaName());
    	geolocation.setFormat(geoLocDto.getFormat());
    	geolocation.setNo(geoLocDto.getNo());
    	geolocation.setSiteID(geoLocDto.getSiteID());
    	geolocation.setContractorNo(geoLocDto.getContractorNo());
    	
    	geolocationDao.insert(geolocation);
    	
    	return Response.ok(geolocation.toDto(false)).build();
    }

    @GET
    @Path("/geolocation/")
    public Response getLocation(@Context HttpServletRequest httpServletRequest,
                                @Context ServletContext servletContext) {

        Optional<String> sessionKey = SessionKeyProvider.getSessionKey(httpServletRequest);
        //if (sessionKey.isPresent() && isSessionValid(sessionKey.get(), servletContext)) {
        List<DbGeolocation> geolocationList = geolocationDao.get();
        List<GeolocationDto> geolocationDtoList = new ArrayList<GeolocationDto>();
        for (DbGeolocation geolocation : geolocationList)
        {
        	GeolocationDto geolocationDto = new GeolocationDto();
        	geolocationDto.setName(geolocation.getName());
        	geolocationDto.setNumberOfScreens(geolocation.getNumberOfScreens());
        	geolocationDto.setAddress(geolocation.getAddress());
        	geolocationDto.setLocation(geolocation.getLocation());
        	geolocationDto.setDistrict(geolocation.getDistrict());
        	geolocationDto.setDistrictNo(geolocation.getDistrictNo());
        	geolocationDto.setSiteNo(geolocation.getSiteNo());
        	geolocationDto.setPanelNo(geolocation.getPanelNo());
        	geolocationDto.setPostcode(geolocation.getPostcode());
        	geolocationDto.setSize(geolocation.getSize());
        	geolocationDto.setPanelID(geolocation.getPanelID());
        	geolocationDto.setLatitude(geolocation.getLatitude());
        	geolocationDto.setLongitude(geolocation.getLongitude());
        	geolocationDto.setEasting(geolocation.getEasting());
        	geolocationDto.setNorthing(geolocation.getNorthing());
        	geolocationDto.setContractor(geolocation.getContractor());
        	geolocationDto.setTVAreaName(geolocation.getTVAreaName());
        	geolocationDto.setAreaCode(geolocation.getAreaCode());
        	geolocationDto.setAreaName(geolocation.getAreaName());
        	geolocationDto.setTVArea(geolocation.getTVArea());
        	geolocationDto.setSubAreaCode(geolocation.getSubAreaCode());
        	geolocationDto.setSiteCode(geolocation.getSiteCode());
        	geolocationDto.setPanelCode(geolocation.getPanelCode());
        	geolocationDto.setEnvironment(geolocation.getEnvironment());
        	geolocationDto.setPanelName(geolocation.getPanelName());
        	geolocationDto.setFrameID(geolocation.getFrameID());
        	geolocationDto.setPanelSizeName(geolocation.getPanelSizeName());
        	geolocationDto.setPanelFullDescription(geolocation.getPanelFullDescription());
        	geolocationDto.setPanelSizeCode(geolocation.getPanelSizeCode());
        	geolocationDto.setPanelSiteAddressPostcode(geolocation.getPanelSiteAddressPostcode());
        	geolocationDto.setPanelGridReferenceGridn(geolocation.getPanelGridReferenceGridn());
        	geolocationDto.setPanelGridReferenceGride(geolocation.getPanelGridReferenceGride());
        	geolocationDto.setPanelPostarFrameID(geolocation.getPanelPostarFrameID());
        	geolocationDto.setPanelPostarMapId(geolocation.getPanelPostarMapId());
        	geolocationDto.setPanelPostarFramePositionId(geolocation.getPanelPostarFramePositionId());
        	geolocationDto.setPanelPostarSubEnvironment(geolocation.getPanelPostarSubEnvironment());
        	geolocationDto.setPanelRef(geolocation.getPanelRef());
        	geolocationDto.setProduct(geolocation.getProduct());
        	geolocationDto.setOrientation(geolocation.getOrientation());
        	geolocationDto.setConurbation(geolocation.getConurbation());
        	geolocationDto.setSubAreaName(geolocation.getSubAreaName());
        	geolocationDto.setFormat(geolocation.getFormat());
        	geolocationDto.setNo(geolocation.getNo());
        	geolocationDto.setSiteID(geolocation.getSiteID());
        	geolocationDto.setContractorNo(geolocation.getContractorNo());
        	
        	geolocationDtoList.add(geolocationDto);
        }
        return Response.ok(geolocationDtoList).build();
                
        //    }
        
        //return Response.status(Response.Status.UNAUTHORIZED).build();
    }
      
    /*private boolean isSessionValid(final String sessionKey, final ServletContext servletContext) {
        if (isDevApp(servletContext)) {
            return true;
        }
        Optional<DbSession> session = Optional.fromNullable(ofy().load().type(DbSession.class).id(sessionKey).now());
        return session.isPresent() && !session.get().isArchived();
    }*/

    private boolean isDevApp(final ServletContext servletContext) {
        return Optional.fromNullable(servletContext.getServerInfo()).transform(new Function<String, Boolean>() {
            public Boolean apply(String s) {
                return s.startsWith("Google App Engine Development/");
            }
        }).or(false);
    }
}
