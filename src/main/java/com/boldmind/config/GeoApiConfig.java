package com.boldmind.config;

public class GeoApiConfig {

    private static final String PROD_KEY = "AIzaSyB_8waw2WdYaeaEC3a3IXOXPKtjxHuwBNk";
    private static final String PREPROD_KEY = "AIzaSyCFvujdonUFvzqhHygallawnLompguuRqg";

    public static String getApiKey(final boolean isProduction) {
        if (isProduction) {
            return PROD_KEY;
        }
        return PREPROD_KEY;
    }
}
