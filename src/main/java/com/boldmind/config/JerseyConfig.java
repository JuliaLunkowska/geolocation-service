package com.boldmind.config;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import com.boldmind.dao.DaoService;
import com.boldmind.dao.GeolocationDao;
import com.boldmind.model.DbGeolocation;
import com.boldmind.session.CORSFilter;
import com.boldmind.util.AppEnvironmentHelper;

public class JerseyConfig extends ResourceConfig {

    static {
        DaoService.register(DbGeolocation.class);
    }

    public JerseyConfig(Class<?>... classes) {
        super(classes);
        init();
    }

    public JerseyConfig() {
        init();
    }

    private void init() {
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(GeolocationDao.class).to(GeolocationDao.class);
                bind(AppEnvironmentHelper.class).to(AppEnvironmentHelper.class);
            }
        });
        register(new CORSFilter());
    }
}
